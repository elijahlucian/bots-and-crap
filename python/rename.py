#! /usr/bin/python3

import os

files = os.listdir()

files_to_rename = []
max_num = 0

for file in files:
    split_file = file.split('.')
    ext = split_file[1]
    if ext != 'jpg':
        continue
    body = split_file[0]
    split_body = body.split('-')
    num = int(split_body.pop(-1))
    if num > max_num:
        max_num = num
    files_to_rename.append({body: split_body, num: num})

digits = len(str(max_num))
