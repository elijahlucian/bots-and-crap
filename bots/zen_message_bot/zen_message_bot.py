import bot_loader
import requests
import json
import time
import logging
from telegram.ext import Updater

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

config = bot_loader.load_config(__file__)
bot_url = "https://api.telegram.org/bot" + config.telegram_bot_token
print(bot_url)

updater = Updater(token=config.telegram_bot_token)

def start(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text="YEEAAAHHH")

start()
