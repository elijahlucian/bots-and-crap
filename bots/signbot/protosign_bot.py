import logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

import urllib
import time
import requests
import json

# takes user messages in telegram and puts them on the sign.