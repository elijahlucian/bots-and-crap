import yaml


def load_config(bot_name):
    # would be cool if you could somehow specify which keys
    # the user wants out of the config file
    # larger scope

    with open("%s.yml" % bot_name.split('.')[0], 'r') as f:
        try:
            config = yaml.load (f)
        except yaml.YAMLError as exc:
            config = 'Your file has errors'
            print('your file has errors')
            print(exc.problem_mark)
            return config, []
    return config


if __name__ == '__main__':
    # for testing
    config = load_config ('pass_test')
    print("TESTING bot_loader.py")
    print("Token:", config['token'])
    for u in config['users']:
        print(u)
