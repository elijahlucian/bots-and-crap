import requests, json, time, urllib
import hmac
from urllib.parse import urlencode
import hashlib
# globals

tradeActive = True
sellLimitReached = False
market = 'btc-xvg' # soon to be argv
closeToGoal = False # this enables the polling switch for the timer from slow to fast.

def accessAPI():
    return True

def getActiveCryptos(r):
    checkList = []
    # print(r)
    print("Current Balances \n")
    for k in r:
        if k['Balance'] > 0:# or k['']:
            print(k['Currency'],' = ',k['Balance'])

            checkList.append(k['Currency'])

    return checkList

def publicAction(action,market):
    if market:
        url = 'https://bittrex.com/api/v1.1/public/' + action + '?market=' + market
    else:
        url = 'https://bittrex.com/api/v1.1/public/' + action

    req = urllib.request.Request(url)
    response = json.loads(urllib.request.urlopen(req).read().decode('utf8'))

    print(response)

    if response["result"]:
        return response["result"]
    else:
        return response["message"]

def accountAction(apikey,secret,action):
    url = 'https://bittrex.com/api/v1.1/account/' + action + '?apikey=' + apikey
    url += '&nonce=' + str(int(time.time()))
    signature = hmac.new(secret.encode('utf-8'), url.encode('utf-8'), hashlib.sha512).hexdigest()
    headers = {'apisign': signature}
    req = urllib.request.Request(url, headers=headers)
    response = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
    if response["result"]:
        return response["result"]
    else:
        return response["message"]

def buyAndSell(action,apikey,secret,market,quantity,rate):
    url = 'https://bittrex.com/api/v1.1/market/' + action + 'limit?apikey=' + apikey
    url += '&market=' + market
    url += '&quantity=' + str(quantity)
    url += '&rate=' + format(rate,'.10f')
    url += '&nonce=' + str(int(time.time()))
    signature = hmac.new(secret.encode('utf-8'), url.encode('utf-8'), hashlib.sha512).hexdigest()
    headers = {'apisign': signature}
    req = urllib.request.Request(url, headers=headers)
    response = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
    # print(response["message"])
    if response["result"]:
        return response["result"]
    else:
        return response["message"]


with open("secrets.json") as secrets_file:
    f = json.load(secrets_file)
    secrets_file.close()
    apikey = f['key']
    secret = f['secret']


# get active cryptos
r = accountAction(apikey,secret,'getbalances')
activeCryptos = getActiveCryptos(r)

#get active cryptos
marketSummary = publicAction('getmarketsummary','btc-xvg')
askPrice = marketSummary[0]['Ask']
sellPrice = askPrice * 1.5

print(askPrice,sellPrice)
# tick = publicAction('getticker','btc-xvg')
# get minimum order.
# buy = buyAndSell('buy',apikey,secret,market,200,askPrice)
# print(buy)
# print(sell)
# print('bought',market,'at',askPrice,'selling at',sellPrice)

while tradeActive == True:
    # get market info for coin
    print("\n loop start \n")
    print("Getting Ticker:")
    tick = publicAction('getticker',market)

    print("Print Ticker Response Data:")
    if(tick == None):
        print("no result given")
        continue

    print(tick)

    if format(tick['Last']) > format(sellPrice,'.10f') and sellLimitReached == False:
        sellLimitReached = True
        upWardTrend = True

    if sellLimitReached == False:
        print("current price and shit")
        print('current price',format(tick['Last'],'.10f'),'Selling at 0.0000111600')#,format(sellPrice,'.10f'))

    elif sellLimitReached == True:
        print("Getting Moving Average - Linear Weighted, printing price:")
        print('current price',format(tick['Last'],'.10f'),'limit was 0.0000111600')#,format(sellPrice,'.10f'))
        # start to get moving average over time with 5% loss ability
        # MA will have 8 samples (3 minutes)

        if upWardTrend == True:
            hardSell = sellPrice * 0.95
            # if price is increasing, set new sellPrice at current moving average - set hardSell to newPrice -10% (allowing for greater possible margin.)
        else:
            x = 0
            # trendAngle = ticksTotal / numberOfTicks * some math shit = sampleAngle
            # when price enters a downward trend, figure out "angle" of downward trend, and make decision, sample over 15s

    #sell = buyAndSell('sell',apikey,secret,market,200,sellPrice)



        # print(tick)

    time.sleep(15)
