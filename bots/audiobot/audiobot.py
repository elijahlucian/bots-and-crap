#!/usr/bin/python3

# possible alternates
# https://github.com/teragonaudio/MrsWatson

import wave, json, struct

wav = './editedtest.wav'

f = wave.open(wav, 'r')

# get total audio frames
totalFrames = f.getnframes()
# get audio channels
totalChannels = f.getnchannels()
# get bit depth?? 1 = 8bit / 2 = 16bit / 3 = 24 bit / 4 = 32 bit
bitDepth = f.getsampwidth()
# get samplerate
sampleRate = f.getframerate()
# get all params
params = f.getparams()

# get starting and ending points of main audio file.
frameStart = ''
frameEnd = ''

# get normalize differential
peakVolume = ''


# readable bit depths for printing
bitDepthIRL = ''
if bitDepth == 1:
    bitDepthIRL = '8 Bits'
elif bitDepth == 2:
    bitDepthIRL = '16 Bits'
elif bitDepth == 3:
    bitDepthIRL = '24 Bits'
elif bitDepth == 4:
    bitDepthIRL = '32 Bits'

#print all frames

nf = f.readframes(totalFrames)
# str(nf).split("'")

print(nf)


#show details of current file
print()
print('File Name: ' + wav)
print()
print('Bit Depth: ' + bitDepthIRL)
print('Sample Rate: ' + str(sampleRate))
print('Total Number Of Frames: ' + str(totalFrames))
print()
print(params)
print()
