#!/usr/bin/python3

import wave

wav = 'test.wav'

with wave.open(wav, 'rb') as f:
    frames = f.readframes(f.getnframes())
    params = f.getparams()

    with wave.open('edited'+wav, 'wb') as l:
        l.setnchannels(f.getnchannels())
        l.setsampwidth(f.getsampwidth())
        l.setframerate(f.getframerate())
        newdata = []
        count = 0
        meatend = False
        meatstart = False

        for i in frames:

            if meatstart == False:
                # wait for detection of loudness
                if i > 15 and i < 230:
                    meatstart = True
                    newdata.append(i)

            elif meatstart == True:
                newdata.append(i)

                if meatend == True:
                    print('Silence has been chopped')
                    break

                if i < 15 or i > 240:
                    #print(i)
                    count += 1;

                    if count > 1000:
                        meatend = True
                else:
                    count = 0;


            #print(meatend)


        #print(bytes(newdata))
        l.setparams(params)
        l.writeframesraw(bytes(newdata))
