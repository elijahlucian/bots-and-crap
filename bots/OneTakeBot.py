import bot_loader
import requests
import json
import time

from google.cloud import firestore

config = bot_loader.load_config(__file__)

TELEGRAM_KEY = config['telegram_bot_token']
TELEGRAM_URL = "https://api.telegram.org/bot%s/" % TELEGRAM_KEY
TELEGRAM_GROUP = {'chat_id': '-1001201847149'}
FIREBASE_KEY = config['firebase_api_key']
FIREBASE_URL = 'https://{}.firebaseio.com'.format(config['firebase_project_id'])
ADMINS = config['admins']  # get firebase users, filter admins

db = firestore.Client()


def get_last_update_id():
    last_update_id = db.collection(u'stats').document(u'telegram')
    return last_update_id.get().to_dict()['last_update_id']


def get_doc(target):
    doc_ref = db.collection(u'{}'.format(target))
    docs = doc_ref.get()

    docs_array = []

    for doc in docs:
        idea = doc.to_dict()
        docs_array.append(idea)

    return docs_array


def write_doc(target):
    return True


def read_chat(line):
    keys = line.split(' ')
    # look for 'idea:' key
    # get text after 'idea:'
    # store idea into database (fire base hopefully)
    # react app to read all the ideas in a formatted fashion
    return


def store_idea(text, context='ideas'):
    doc_ref = db.collection(u'general').document(u'{}'.format(context))
    old_ideas = doc_ref.get().to_dict()['list']
    ideas_array = old_ideas + [text]
    doc_ref.set({u'list': ideas_array})

    reply = "'{}' has been stored in the database".format(text)
    # send_message(reply)
    print(reply, context)
    return


def set_last_update_id(message_id):
    doc_ref = db.collection(u'stats').document(u'telegram')
    doc_ref.set({u'last_update_id': message_id})
    return


def check_message(message):
    split_message = message['text'].split(':')
    if len(split_message) > 1:
        key = split_message[0].lower()
        text = split_message[1].strip()
        if key == 'idea':
            print("storing idea")
            store_idea(text)
        elif key == 'todo':
            print("storing todo")
            store_idea(text, 'todos')
    return


def get_updates(last_update_id):
    print(last_update_id)
    updates = api('getUpdates?offset={}'.format(last_update_id + 1))['result']

    for u in updates:

        try:
            message = u['message']
        except KeyError:
            # send_message("Tanner, stop fucking with me, motherfucker.")
            continue

        message = u['message']
        if u['update_id'] > last_update_id:
            last_update_id = u['update_id']

        if 'new_chat_participant' in message:
            # print("a new challenger appears! and it's %s" % message['new_chat_participant']['username'])
            pass
        elif 'text' in message:
            # print("{} - {}: {}".format(message['message_id'], message['from']['username'], message['text']))
            check_message(message)
        else:
            pass
            # print(message)
    set_last_update_id(last_update_id)
    return last_update_id


def send_message(text):
    response = api('sendMessage', text, post=True)
    print(response)
    return


def api(endpoint, text='', post=False):
    url = TELEGRAM_URL + endpoint
    print('sending', text, 'to', url)
    if post:
        url += "?text={}&chat_id={}".format(text, TELEGRAM_GROUP['chat_id'])
        r = requests.post(url)
    else:
        r = requests.get(url)
    return json.loads(r.text)


def start_bot():
    # send_message('FEAR MEH!')

    sleep_length = 15
    last_update_id = get_last_update_id()

    while True:
        prev_id = last_update_id
        last_update_id = get_updates(last_update_id)
        if prev_id == last_update_id:
            sleep_length += 15
        else:
            sleep_length = 15
        print("sleep time is {}".format(sleep_length))
        time.sleep(sleep_length)
    return


if __name__ == '__main__':
    start_bot()
