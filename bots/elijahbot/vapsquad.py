base_url = 'http://localhost:3000'

def new_idea(body):
    keyword = '!idea'
    # persist idea into notes

def add_randomized_content(kind, body):
    keyword = '!random - [kind]: [body]'
    requests.post(base_url + '/randomizer/kind', data=body)

def post_segment(body)
    keyword = '!segments - new - [body]'
    # add segment to show selector
    requests.post(base_url + '/segments', data=body)

def put_actor(body)
    keyword = '!update - [attribute]: [body]'
    # update bio
    requests.put(base_url + '/actors/:id', data=body)

def post_actor(body)
    keyword = '!addme'
    # interactive with actor [name, handle, tagline]
    # add new actor w/bio
    requests.post(base_url + '/actors', data=body)

