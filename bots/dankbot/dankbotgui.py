import pyforms
from pyforms import BaseWidget
from pyforms.Controls import ControlText
from pyforms.Controls import ControlButton

class SimpleExample1(BaseWidget):

    def __init__(self):
        super(SimpleExample1,self).__init__('Simple Example 1')

        #definiton of the forms fields
        self._firstname = ControlText ('First Name', 'Default value')
        self._middlename = ControlText ('Middle Name')
        self._lastname = ControlText('Lastname Name')
        self._fullname = ControlText('Full Name')
        self._button = ControlButton('Press This Button')

if __name__ == "__main__": pyforms.start_app( SimpleExample1 )