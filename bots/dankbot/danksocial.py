#!/usr/bin/env python3

import tweepy, time, datetime, json, sys, random
from time import sleep
from credentials import *

#Hello I am your new social bot!
#I will post once per week to promote twitch shows
#I will do shit on social media, like tweets, and otherwise be awesome.

with open('/home/pi/dankbot/serverinfo/serverinfo.json') as data_file:    
	data = json.load(data_file)

#twitter
consumer_key = data["CONSUMER_KEY_HERE"]
consumer_secret = data["CONSUMER_SECRET_HERE"]
access_token = data["ACCESS_TOKEN_HERE"]
access_token_secret = data["ACCESS_TOKEN_SECRET"]

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
twitterapi = tweepy.API(auth)

homedir = "/home/pi/dankbot/"

hashtaglist = open(homedir + 'hashtags.txt', 'r').read().splitlines()

def tweety(content):
    twitterapi.update_status(content)
    return

def favhash(hashtag,maxnum,sleeptime,sleeptimehi):
    for tweet in tweepy.Cursor(twitterapi.search,q=hashtag).items(maxnum):
        print("favoriting "+hashtag)
        try:
            print('\nTweet by: @' + tweet.user.screen_name)
            tweet.favorite()
            print('favorited!')
        except tweepy.TweepError as e:
            print(e.reason)
        except StopIteration:
            break
        sleep(random.randint(sleeptime,sleeptimehi))


while 1:

    for h in hashtaglist:
        favhash(h,15,30,60)

    sleep(300)

