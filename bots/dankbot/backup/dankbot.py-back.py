#!/usr/bin/env python

import socket
import sys
import subprocess
import os
import json
import pygame
import time
import bs4
import requests
import random
from glob import glob

pygame.mixer.init(frequency=44100, size=-16, channels=1, buffer=4096)

#establish working directories
homedir = '/home/pi/dankbot/'
dirsb = '/home/pi/soundboard/'
counter = time.time()

with open('/home/pi/dankbot/serverinfo/serverinfo.json') as data_file:    
    data = json.load(data_file)

server = data["server"]
channel = data["channel"]
nickname = data["nickname"]
password = data["password"]
clientid = data["clientid"]
clientsecret = data["clientsecret"]
irc = socket .socket(socket.AF_INET, socket.SOCK_STREAM)

#load soundboard filenames but not subdirectories
soundlist = [f for f in os.listdir(dirsb) if os.path.isfile(os.path.join(dirsb, f))]
soundlist.sort()

#list subfolders
dankMemes = [p[20:-1] for p in glob(dirsb+'*/')]

#get Emote Names
dankEmote = [l for l in dankMemes if 'dank' in l]

#get sub names
subfile = open(homedir + 'subs.txt', "r")
dankSubs = subfile.read().splitlines()

#load dank memes soundboard


		#print dankFile

# dirToffee = '/home/pi/soundboard/dankToffee/'
# dirNotporn = '/home/pi/soundboard/dankNotporn/'
# dankToffee = os.listdir(dirToffee)
# dankNotporn = os.listdir(dirNotporn)
# sbdankToffee = {}
# sbdankNotporn = {}

def dankConnect():
	irc.connect((server, 6667))
	irc.send("USER "+ nickname + "\n")
	irc.send("PASS "+ password +"\n")
	irc.send("NICK "+ nickname + "\n")
	irc.send("JOIN "+ channel +"\n")
	return

dankConnect()

# load all the soundboards
soundboard = {}

def loadsoundboard():
	for sounditem in soundlist:
		soundboard[sounditem] = pygame.mixer.Sound(dirsb+sounditem)

		with open('/var/www/html/soundboard/index.html','w') as file:
			file.write('<html><link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"><link rel="stylesheet" href="./style.css" /><body><p>')
			file.writelines(['<div class="emote">' + emoteName + '</div>' for emoteName in dankEmote])			
			for memeName in set(dankMemes).difference(dankEmote):
				file.writelines(['<div class="memes">' + memeName + '</div>'])
			file.writelines(['<div class="sound">!sb ' + sound[0:-4] + '</div>' for sound in soundlist])
			file.write('</p></body></html>')
	return
loadsoundboard()

# def loadmemes():
# 	for foldername in dankMemes:
# 		dankFolder = os.listdir(dirsb+foldername)
# 		for soundname in dankFolder:
# 			#create dank memes soundboard
# 			sbdankMemes[soundname]=pygame.mixer.Sound(dirsb+foldername+'/'+soundname)
# 			dankFile=dirsb+foldername+'/'+soundname
# 	return
# loadmemes()

'''
#-----------------
def loaddankToffee():
	for toffsound in dankToffee:
		sbdankToffee[toffsound] = pygame.mixer.Sound(dirToffee+toffsound)		
	return

def loaddankNotporn():
	for notpornsound in dankNotporn:
		sbdankNotporn[notpornsound] = pygame.mixer.Sound(dirNotporn+notpornsound)
	return
#-----------------
def loadWutFace():
	for WutFacesound in WutFace:
		sbWutFace[WutFace] = pygame.mixer.Sound(dirWutFace+WutFacesound)
loaddankNotporn()
loaddankToffee()
'''

#get followers subs etc...

def getsubs():
    return

def getfollowers():
    return

def takepicandtweet():
    return

while 1:
	
	buffer = 2048
	text = irc.recv(buffer)
	# print text

	if time.time() - counter > 300:
		output = 'Make some nooooise!'
		irc.send('PRIVMSG '+ channel +' :' + output + '\n')
		counter = time.time()
		continue 	

	if text is None:
		dankConnect()
		continue

	if text.find('PRIVMSG') == -1:
		print text
		continue

	parts = text.rstrip().split(':', 2)
	#print parts
	
	messagebody = parts[2]
	words = messagebody.split()
	print messagebody
	print words

	namestr = parts[1]
	namepart = namestr.split('!', 1)
	name = namepart[0]

	print '>> ' + name + ': ' + messagebody

	if messagebody.find('!sb ') != -1:
		messagesplit = messagebody.split(' ', 1)
		soundfile = messagesplit[1]
		wavename = soundfile+".wav"
		if soundfile == 'rando':
			soundboard[random.choice(soundlist)].play()
			continue
		if wavename in soundlist:
			soundboard[wavename].play()
		else:
			output = "That sound is not in our soundboard, visit vapsquad.club:8080/soundboard/ to see our list or request one with a donation over $5. protip: start your message with the SB command"
			irc.send('PRIVMSG '+ channel +' :' + output + '\n')
			counter = time.time()
		continue

	for dankMeme in words:
		if dankMeme in dankMemes:
			print dankMeme
			dankBoard = {}
			dankList = [f for f in os.listdir(dirsb+dankMeme+'/') if os.path.isfile(os.path.join(dirsb+dankMeme+'/', f))]
			dankFolder = os.listdir(dirsb+dankMeme+'/')
			for dankItem in dankFolder:
				dankBoard[dankItem] = pygame.mixer.Sound(dirsb+dankMeme+'/'+dankItem)
				print dankItem
			#pick random file
			dankBoard[random.choice(dankList)].play()
			print dankBoard
			print random.choice(dankList)
			
		if dankMeme in dankMemes:
			break

	if messagebody.find('!renew list') != -1:
		output = "Yes maaaasta! Soundboards have been re-initialized!"
		loadsoundboard()
		loadmemes()
		irc.send('PRIVMSG '+ channel +' :' + output + '\n')   
		continue

	if messagebody.find('!saycheese') != -1:
		#get camera to take pic and upload to twitter
		print 'fuck yeah'
		continue

	if messagebody.find('PING') != -1:
    		irc.send('PONG ' + text.split() [1] + '\r\n')
		
	if messagebody.find('!uptime') != -1:
		output = subprocess.check_output("uptime", shell=True) 
		irc.send('PRIVMSG '+ channel +' :' + output + '\n')


