#!/usr/bin/env python

import socket
import sys
import subprocess
import os
import json
import pygame
import time
import bs4
import requests
import random

emojis = {}

with open('subEmoji.json') as emojiStuff:
    emojis = json.load(emojiStuff)

    sb = '#!/usr/bin/env python\n\n' \
    + 'import socket\n' \
    + 'import sys\n' \
    + 'import subprocess\n' \
    + 'import os\n' \
    + 'import json\n' \
    + 'import pygame\n' \
    + 'import time\n' \
    + 'import bs4\n' \
    + 'import requests\n' \
    + 'import random\n\n' \
    + 'messagebody = ""\n\n' \
    + 'pygame.mixer.init(frequency=44100, size=-16, channels=1, buffer=4096)\n'

    for emIndex, emDir in emojis.items():
        sb += '\nsb' + emIndex + ' = {}\n' \
        + 'dir' + emIndex + ' = "' + emDir + '"\n' \
        + '' + emIndex + ' = os.listdir(' + 'dir' + emIndex + ')\n\n' \
        + 'def load' + emIndex + '():\n' \
        + '\tfor emojiSound in ' + emIndex + ':\n' \
        + '\t\tsb' + emIndex + '[emojiSound] = pygame.mixer.Sound(dir' + emIndex + ' + emojiSound)\n' \
        + '\treturn\n\n' \
        + 'load' + emIndex + '()\n\n' \
        + 'if messagebody.find("' + emIndex + '") != -1:\n' \
        + '\tsb' + emIndex + '[random.choice(' + emIndex + ')].play()\n'
        #+ '\tcontinue\n'
        #exec sb
        #sbdankNotporn[random.choice(dankNotporn)].play()

    with open('randolph.py', 'w') as outfile:
        outfile.write(sb)
