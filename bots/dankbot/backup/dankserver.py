#/usr/bin/env python

from flask import Flask, request, current_app
app= Flask(__name__, static_url_path='')

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/soundboard/')
def soundboard():
    return current_app.send_static_file("soundboard.html")

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8081)