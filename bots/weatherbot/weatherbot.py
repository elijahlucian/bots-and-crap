import logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

import urllib
import time
import requests
import json

# grab random weather endpoint
# show wearher from random place aroud the workd

while True:
    try:
        res = requests.get('https://catfact.ninja/fact')
        j = json.loads(res.content.decode('utf-8'))
        joke = j['fact']
        joke = joke.replace('“', '"')
        joke = joke.replace('”', '"')
        joke = joke.replace('’', '\'')
        joke = joke.replace('°', '')
        res.connection.close()

        trolltext = urllib.parse.urlencode({'text' : 'Incoming Cat Fact:'})
        requests.get('http://www.protospace.ca/sign/?' + trolltext)
        time.sleep(2)

        joke_array = joke.split(' ')

        while joke_array:
            section_array = []
            while joke_array and len(' '.join(section_array) + joke_array[0]) < 39:
                section_array += [joke_array.pop(0)]
            section = ' '.join(section_array)

            trolltext = urllib.parse.urlencode({'text' : section})
            print(section)

            requests.get('http://www.protospace.ca/sign/?' + trolltext)
            time.sleep(2)

        print('')
        time.sleep(600)
    except KeyboardInterrupt:
        exit()
    except BaseException as e:
        print('Error: ' + str(e))

    time.sleep(0.1)
