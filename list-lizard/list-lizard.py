# turn your ascii artwork into a python list!
# usage command-line:
# $ python3 ascii-text-to-list.py input.txt [default: "list" / "logo"]
# will output input-list.txt

import sys

try:
    parse_type = sys.argv[2]
except IndexError:
    parse_type = "list"

input_file = sys.argv[1]
new_filename = input_file[:-4] + '-list.txt'
output_list = []

with open(input_file) as f:
    source = f.read()

split_source = source.split('\n')
# split_source = split_source.split('\t')
print(split_source)
with open(new_filename, 'w') as f:
    f.write('list = [\n')
    temp = 0
    for line in split_source[:-2]:
        if len(line) < 3:
            continue
        while True:
            if line[0] == " ":
                line = line[1:]
            elif line[-1] == " ":
                line = line[0:-1]
            else:
                break
        # if "\" in line:
            # line.replace("\" with "\\")
        print(">"+line+"<")

        if parse_type == "logo":
            space_at_line_end = ' ",'
        else:
            space_at_line_end = '",'

        line = '\t"' + line + space_at_line_end + '\n'
        f.write(line)
        temp += 1
    f.write(']')
