function msg(m)
  return RPR_ShowConsoleMsg(tostring(m) .. "\n")
end

lastRecordingState = -1

function CheckForRecordingState()
  recordingState = reaper.GetToggleCommandState(1013) -- action: "Transport: Record", return values: 0 = recording stopped, 1 = recording started

  if lastRecordingState ~= recordingState then
    msg("Recording state: " .. recordingState)
  end

  lastRecordingState = recordingState

  reaper.defer(CheckForRecordingState) -- continiously calls itself
end

CheckForRecordingState() -- initially call the function
