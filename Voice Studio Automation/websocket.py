import websocket

ws = websocket.create_connection("http://192.168.1.79:8080")

print("Sending 'Hello, World'...")
ws.send("Hello, World")
print("Sent")
print("Receiving...")
result =  ws.recv()
print("Received '%s'" % result)
ws.close()
