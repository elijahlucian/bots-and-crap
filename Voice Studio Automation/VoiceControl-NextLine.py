# studio controller for Reaper
import json
import time
from reaper_python import *
from sws_python import *
import sys
import urllib.request
import os
sys.argv=["Main"]

import sourceModule as sm

def CheckInit(o):
    directory = sm.workingDirectory + o["userName"] + '-' +  o["scriptName"] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

def Main():
    RPR_PreventUIRefresh(1)
    o = sm.getCurrentLineObject()

    CheckInit(o)
    # sm.Msg(str(o))

    ind = o['lineNum']
    if ind < o['scriptLength']:
        if RPR_GetPlayState() !=5:
            sm.tRtz()
            sm.tRec()
        else:
            sm.nextLine(o)
    else:
        sm.tStop()
        # sm.Msg(str(o['scriptEnd']))
        # sm.Msg("You have reached the end of your lines!")

Main()

RPR_PreventUIRefresh(-1)
RPR_UpdateArrange()
