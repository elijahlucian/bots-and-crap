import json
import urllib.request
from reaper_python import *
from sws_python import *
import time
import sys
import os
import subprocess
import shutil


# variables

projectName = RPR_GetProjectName(0,'',512)
workingDirectory = 'C:/Users/elija/Documents/makerFaire2017/vapTown/soundboard/'

# workingDirectory = 'U:/pi/vapTown/soundboard/'
# workingDirectory = 'Y:/pi/test/'
url = 'http://192.168.1.66:8080/api/'

# Reaper Things
def Msg(p):
    RPR_ShowConsoleMsg(p + "\n")

def Cmd(x,y):
    RPR_Main_OnCommand(x,y)

def tRec():
    Cmd(1013, 0)

def tStopSave():
    Cmd(40667,0)

def tStopDel():
    Cmd(40668,0)

def tEnd():
    Cmd(40043,0)

def tDel():
    Cmd(40184,0)

def tRtz():
    Cmd(40042,0) #go to start of project

def tStop():
    RPR_OnStopButton()

def selectLast():
    RPR_SelectAllMediaItems(0,True)
    tot = RPR_CountSelectedMediaItems(0)
    item = RPR_GetMediaItem(0, tot)
    RPR_SetMediaItemSelected(item, True)

## url and api shit

def userAndScriptName(o):
    return o["userName"] + '-' + o["scriptName"] + '/'

def getUrl():
    url = 'http://192.168.1.66:8080/api/'
    return url

def api(x):
    return urllib.request.urlopen(x)

def apiControl(x):
    control = url + "control?id=" + x
    print(control)
    return api(control)

def apiAltTake():
    x = api(url + "takeNumber")
    data = x.read()
    o = json.loads(data)
    return str(o["takeNumber"])

def getCurrentLineObject():
    u = url + "line"
    apiFileName = api(u)
    data = apiFileName.read()
    o = json.loads(data)
    return o

def checkEndOfScript(o):
    return o['scriptEnd']

def apiAltTake():
    x = api(url + "takeNumber")
    data = x.read()
    t = json.loads(data)
    return str(t["takeNumber"])
#file system shit

def saveTake(o,takeNumber):
    tStopSave() # Transport: Stop (save all recorded media)
    clip = selectLast()
    nameClip(o,takeNumber) # rename clip
    tEnd() # go to end of media
    tRec() # begin recording

def nameClip(o,takeNumber):
    item = RPR_GetSelectedMediaItem(0, 0)
    take = RPR_GetMediaItemTake(item, RPR_CountTakes(item) - 1)
    track = RPR_GetMediaItemTrack(item)
    oldSource = RPR_GetMediaItemTake_Source(take)
    oldFilename = RPR_GetMediaSourceFileName(oldSource, '', 512)
    tDel()
    f = o["lineFile"]
    newFilename = workingDirectory + o["userName"] + "-" + o["scriptName"] + "/" + f + '.ogg'

    #add file exists logic and add numbered takes
    # if not os.path.exists(directory):
    #     os.makedirs(directory)
    ################ CHECK IF YOUR MOM IS HOT #########################

    theInt = 1
    yourMom = "not"
    try:
        # os.rename(oldFilename[1], newFilename) # rename the files
        silenceCommand = "C:/Program Files (x86)/sox-14-4-2/sox.exe " + '"' + oldFilename[1] + '"' + " " + newFilename + "  reverse silence -l 1 0.1 1%% reverse silence  -l 1 0.1 1%% "
        # Msg(silenceCommand)
        subprocess.call(silenceCommand)

        # shutil.move(oldFilename[1], newFilename) # move file
    except Exception:
        # Msg("file already exists,yo")
        while yourMom != "hot":
            newFilename = workingDirectory + o["userName"] + "-" + o["scriptName"] + "/" + f + '-' + str(theInt) + '.wav'
            try:
                shutil.move(oldFilename[1], newFilename) # move file
                yourMom = "hot"
                break;
            except Exception:
                theInt += 1

    ################# DANG, YOUR MOM IS HOT ###########################
    # silenceShit(newFilename)
    # newSource = RPR_PCM_Source_CreateFromFile(newFilename)
    #Cmd(40100,0)

    # RPR_SetMediaItemTake_Source(take, newSource)
    #os.rename(reaperFileName, scriptFileName)

# def silenceShit(f,n):
#     # Msg(f)
#     fOgg = f[:-3] + "ogg"
#     silenceCommand = "C:/Program Files (x86)/sox-14-4-2/sox.exe " + f + " " + fOgg + "  reverse silence -l 1 0.1 1%% reverse silence  -l 1 0.1 1%% "
#     # Msg(silenceCommand)
#     subprocess.call(silenceCommand)

# script specific shit

def nextLine(o):

    # item = RPR_GetSelectedMediaItem(0, 0)
    # take = RPR_GetMediaItemTake(item, RPR_CountTakes(item) - 1)
    # mediaLength = RPR_GetMediaSourceLength(take, True) # check length of clip, and if more than x do shit
    # Msg(str(mediaLength))

    saveTake(o,o["takeNumber"])
    apiControl("nextLine") # hit up server for nextLine
    RPR_SelectAllMediaItems(0,True)
    Cmd(40184,0) # delete all items

def altTake(o):
    saveTake(o,o["takeNumber"])

def prevLine():
    apiControl("prevLine") # hit server for prevLine

def clearProject():
    RPR_SelectAllMediaItems(0,True)
    Cmd(40184,0) # delete all items

def reTake():
    tStopDel() # Transport: Stop (DELETE all recorded media)
    tEnd() # puts cursor to end of clips
    tRec() # starts recording
