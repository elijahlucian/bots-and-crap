# Voice Control Script - Previous Line - Elijah Lucian
from reaper_python import *
from sws_python import *
import sys
import urllib.request
sys.argv=["Main"]

import sourceModule
url = sourceModule.getUrl()

def api(x):
    return urllib.request.urlopen(x)

def apiControl(x):
    control = url + "control?id=" + x
    return api(control)

def Msg(p):
    RPR_ShowConsoleMsg(p + "\n")

def prevLine():
    apiControl("prevLine") # hit server for prevLine

def Main():
    RPR_PreventUIRefresh(1)
    prevLine()

Main()

RPR_PreventUIRefresh(-1)
RPR_UpdateArrange()
