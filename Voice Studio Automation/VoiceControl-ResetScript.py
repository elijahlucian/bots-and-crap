# studio controller for Reaper
import json
import time
# from reaper_python import *
# from sws_python import *
import sys
import urllib.request
import os
sys.argv=["Main"]

import sourceModule as sm

def Init():
    # make this a function in sourceModule
    directory = sm.workingDirectory
    if not os.path.exists(directory):
        os.makedirs(directory)

def Main():
    resetApi = sm.url + "reset"
    sm.clearProject()
    sm.api(resetApi)
    sm.Cmd(40668,0) #stop
    sm.Cmd(40042,0) #go to start of project

Init()
Main()
