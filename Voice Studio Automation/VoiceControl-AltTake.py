# studio controller for Reaper
import json
from reaper_python import *
from sws_python import *
import sys
import urllib.request
import os
sys.argv=["Main"]

#variables
import sourceModule as sm

def Main():
    RPR_PreventUIRefresh(1)
    o = sm.getCurrentLineObject()
    sm.altTake(o)

Main()

RPR_PreventUIRefresh(-1)
RPR_UpdateArrange()
