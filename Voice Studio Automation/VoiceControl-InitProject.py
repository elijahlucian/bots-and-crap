# studio controller for Reaper
import time
import sys
import os
from reaper_python import *
from sws_python import *
sys.argv=["Main"]

import sourceModule as sm
url = sm.getUrl()

def Init():
    o = sm.getCurrentLineObject()
    if o["userName"] == None:
        sm.Msg("Enter a username, no project has been created.")
    else:
        directory = sm.workingDirectory + o["userName"] + '-' +  o["scriptName"] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
            sm.Msg("initialized and Folder Created: " + directory)
        else:
            sm.Msg("Enter a unique username")

Init()
