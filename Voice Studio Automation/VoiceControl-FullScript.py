# studio controller for Reaper
import json
import time
from reaper_python import *
from sws_python import *
import sys
import urllib.request
import os
sys.argv=["Main"]

#variables
import sourceModule
url = sourceModule.getUrl()

proj = RPR_GetProjectName(0,'',512)
#workingDirectory = 'C:/Users/elija/Documents/programming/Reaper/' + proj[1][:-4] + '/'
workingDirectory = sourceModule.workingDirectory()

def Init():

    directory = workingDirectory
    if not os.path.exists(directory):
        os.makedirs(directory)

    print("initialized")
    # add logic for start and end of scripts.

    # if start of script:
        # set nextLine to just record and not try and rename etc

    # if end of script:
        # set msg to say "recording finished!" plus a message on the daw and stop recording altogether

def api(x):
    return urllib.request.urlopen(x)
    #apiControl = urllib.request.urlopen('http://192.168.1.79:8080/api/control?=')

def apiControl(x):
    control = url + "control?id=" + x
    return api(control)

def apiAltTake():
    return api(url + "takeNumber")

def init():
    x = 1
    #add all the functions here for readability

def Cmd(x,y):
    RPR_Main_OnCommand(x,y)

def tRec():
    Cmd(1013, 0)

def tStopSave():
    Cmd(40667,0)

def tStopDel():
    Cmd(40668,0)

def tEnd():
    Cmd(40043,0)

def tDel():
    Cmd(40184,0)

def tRtz():
    Cmd(40042,0) #go to start of project

def tStop():
    RPR_OnStopButton()

def countClips():
    RPR_SelectAllMediaItems(0,True)
    tot = RPR_CountSelectedMediaItems(0)
    RPR_SelectAllMediaItems(0,False)
    return tot

def selectLast():
    RPR_SelectAllMediaItems(0,True)
    tot = RPR_CountSelectedMediaItems(0)
    item = RPR_GetMediaItem(0, tot)
    RPR_SetMediaItemSelected(item, True)

def getScriptFileName(o):
    return o["lineFile"]

def trimRecording():
    print("trim last recording")
    # trims the recordings
    # trims clip to front and end of speech (also possibility of batch process at the end of project)

def nameClip(o):
    # get reaperFileName

    # get project name

    item = RPR_GetSelectedMediaItem(0, 0)
    take = RPR_GetMediaItemTake(item, RPR_CountTakes(item) - 1)
    track = RPR_GetMediaItemTrack(item)

    oldSource = RPR_GetMediaItemTake_Source(take)
    oldFilename = RPR_GetMediaSourceFileName(oldSource, '', 512)

    #Cmd(40101,0)

    tDel()
    f = getScriptFileName(o)
    newFilename = workingDirectory + f + '.wav'

    #add file exists logic and add numbered takes
    # if not os.path.exists(directory):
    #     os.makedirs(directory)

    os.rename(oldFilename[1], newFilename)
    newSource = RPR_PCM_Source_CreateFromFile(newFilename)
    #Cmd(40100,0)

    RPR_SetMediaItemTake_Source(take, newSource)
    #os.rename(reaperFileName, scriptFileName)

def saveTake(o):
    tStopSave() # Transport: Stop (save all recorded media)
    clip = selectLast()
    # trimRecording()
    nameClip(o) # rename clip
    tEnd() # go to end of media
    tRec() # begin recording

def reTake():
    tStopDel() # Transport: Stop (DELETE all recorded media)
    tEnd() # puts cursor to end of clips
    tRec() # starts recording

def nextLine(o):
    saveTake(o)
    apiControl("nextLine") # hit up server for nextLine

def prevLine(o):
    saveTake(o)
    apiControl("prevLine") # hit server for prevLine

def getCurrentLineObject():
    u = url + "line"
    apiFileName = api(u)
    data = apiFileName.read()
    o = json.loads(data)
    return o

def checkEndOfScript(o):
    return o['scriptEnd']

def openProject():
    api(url)

def Main():
    RPR_PreventUIRefresh(1)
    o = getCurrentLineObject()

    if o['scriptEnd'] == False:
        if RPR_GetPlayState() !=5:
            tRtz()
            tRec()
        else:
            nextLine(o)
    else:
        tStop()
        Msg("You have reached the end of your lines!")

    # keep looping and waiting for shit

    projectFinished = True
    while projectFinished == False:
        break;

Init()
Main()

RPR_PreventUIRefresh(-1)
RPR_UpdateArrange()
